package com.AtosTrainee.MateuszWarzyc;

import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;

public class QuadraticFunction {

	private static Map<String, Double> getFactors() {
		Scanner scanner = new Scanner(System.in);
		Map<String, Double> factors = new HashMap<String, Double>();

		try {
			System.out.println("Prosz� poda� wsp�czynnik A funkcji kwadratowej: ");
			factors.put("A", scanner.nextDouble());

			System.out.println("Prosz� poda� wsp�czynnik B funkcji kwadratowej: ");
			factors.put("B", scanner.nextDouble());

			System.out.println("Prosz� poda� wsp�czynnik C funkcji kwadratowej: ");
			factors.put("C", scanner.nextDouble());
		} catch (InputMismatchException e) {
			System.out.println("Podano nieodpowiedni� warto��, prosz� spr�bowac ponownie");
			System.exit(0);
		}
		System.out.println("Wz�r funkcji: " + factors.get("A") + "^2x" + factors.get("B") + "x " + factors.get("C"));
		scanner.close();
		return factors;
	}

	private static double calculateDelta(double a, double b, double c) {

		double result = Math.pow(b, 2) - (4 * a * c);
		System.out.println("Delta wynosi: " + result);
		return result;

	}

	private static void clculateZeros(double delta, double a, double b, double c) {
		if (delta > 0) {
			double x1 = (-b + Math.sqrt(delta)) / (2 * a);
			double x2 = (-b - Math.sqrt(delta)) / (2 * a);
			System.out.println("Delta jest wi�ksza od zera i ma 2 miejsca zerowe");
			System.out.println("x1 = " + x1);
			System.out.println("x2 = " + x2);
		} else if (delta == 0) {
			double x0 = (-b) / (2 * a);
			System.out.println("Delta jest r�wna zeru i ma jedno miejsce zerowe");
			System.out.println("x0 = " + x0);
		} else {
			System.out.println("Delta jest mniejsza od zera i nie ma miejsc zerowych");
		}
	}

	public static void calculateQuadraticFunction() {

		Map<String, Double> factors = getFactors();
		double delta = calculateDelta(factors.get("A"), factors.get("B"), factors.get("C"));
		clculateZeros(delta, factors.get("A"), factors.get("B"), factors.get("C"));
	}

}
